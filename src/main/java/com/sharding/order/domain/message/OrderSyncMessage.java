package com.sharding.order.domain.message;


import com.sharding.order.common.enums.OrderOperateType;
import com.sharding.order.common.enums.RequestSource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderSyncMessage implements Serializable {
    /**
     * 请求来源
     */
    private RequestSource requestSource;
    /**
     * 操作类型
     */
    private OrderOperateType orderOperateType;
    /**
     * 订单号
     */
    private String orderNo;
}
