package com.sharding.order.constant;

/**
 * @author ruyuan
 * @Description 响应状态码
 */
public class ResponseCode {

    /**
     * 成功状态
     */
    public static final String SUCCESS = "100";
    /**
     * 异常状态
     */
    public static final String ERROR = "999";

    private ResponseCode() {

    }
}
